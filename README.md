![Alt text](https://github.com/JordanMicahBennett/CONSCIENCIA/blob/master/source-code/data/images/captures/0.png?raw=true"default page")
============================================
============================================





CONSCIENCIA
===========
A language that confluences computation and common sense
http://www.consciencia-language.tk/




HOW WAS CONSCIENCIA SYNTHESIZED? 
===========
Whence consciencia's underylings (grammer,lexer)
were forged in use of language building tools (lex,cup), 
consciencia's user end was composed in a deep graphics frame work [uni-code(tm)](https://github.com/JordanMicahBennett/UNI_CODE-DEEP-UI-ENGINE)


TRADITIONAL SYNTAX TREE
===========
![Alt text](https://github.com/JordanMicahBennett/CONSCIENCIA/blob/master/source-code/data/images/captures/2.png?raw=true"default page")





CLEAN COMFORTABLE USER END
===========
Pure white frontent, powered by (uni-code(tm)).




CLEAN COMPUTE WINDOW
===========
Consciencia's compute window (accessed via build button toggle in core screen)
is written in uni-code(tm)'s deep graphics framework. 
All output and computations blend into consciencia's compute window



![Alt text](https://github.com/JordanMicahBennett/CONSCIENCIA/blob/master/source-code/data/images/captures/1.png?raw=true"default page")



CONSCIENCIA COMMANDS
===========
basic operations
[+]Summate amongst exp1 and exp2. [return sum]
[-]Differ amongst exp1 and exp2. [return difference]
[*]Produce amongst exp1 and exp2.  [return product]
[/]Quote amongst exp1 and exp2. [return quotient]
[%]Modulate amongst exp1 and exp2. [return boolean] 
[%]Expound amongst exp1 and value. [return exponent] 
[>]Trancends? amongst exp1 and exp2. [return boolean] ( is exp1 greater than exp2? )
[<]Relents? amongst exp1 and exp2. [return boolean] ( is exp1 lesser than exp2? )
  
messages
 [Descriptions]
 [show message line] Display Obama will obviously win this race.
 [show message next line] Subsequently display Obama will obviously win this race.
 [show message box] Visually display Obama will obviously win this race.


  
variables
 [DEFINITIONS EXAMPLE- REBINNDING]
 [SINGULAR]
 [Descriptions-standard Definitions]
 [Definition-int] Define variableName as 1.
 [Definition-string] Define variableName as <value>.
 [Definition-double] Define variableName as 10.
 [Definition-float] Define variableName as .0405.
 [Definition-long] Define variableName as 121211213.
 [Definition-long] Define variableName as [1,2,3].
 
 [PLURAL EXAMPLE]
 [Descriptions-standard Definitions, with values]
 [Definition-int-plural-two] Define variableName1 and variableName2 as 1 and 2 respectively
 [Definition-int-plural-multiple] Define variableName1, variableName2, and variableName3 as 1, 2, and 3 respectively
 

unique statements
	statement 0:
	~Display interactive automata (will be computable soon, interactive simulation only for now)
	"Find simulation of cellular automata."


	statement 1:
	~Display partial-consciousness equation.
	"Find computation of consciousness."


	statement 2:
	~Display interactive operating system simulation (will be computable soon, interactive simulation only for now)
	"Find simulation of operating system."
	
	
	statement 3:
	"Find weight of someone <value> feet <value> inch."




AUTHOR PORTFOLIO
============================================
http://folioverse.appspot.com/
